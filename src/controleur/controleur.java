package controleur;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class controleur {
    
            int nbadult = 0;
            int nbetud = 0;
            int nbenf = 0;
            boolean promo = false;
            float montant = 0;
            float prixmoy = 0;
    
               public void calculer(){
                   float total = 7f * nbadult + 5.5f * nbetud + 4f * nbenf;
                   
                   if( promo ) { total=total*0.8f;}
                   
                   int nbPlaces = nbetud + nbenf + nbadult;
                   
                   float prixmoyP;
                   
                   if (nbPlaces>0){prixmoyP=total/nbPlaces;}else{prixmoyP=0f;}
                   
                   setMontant(total);
                   setPrixmoy(prixmoyP);
                    
               }
   
   


            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////         
            
            
    public static final String PROP_NBADULT = "nbadult";

    public int getNbadult() {
        return nbadult;
    }

    public void setNbadult(int nbadult) {
        int oldNbadult = this.nbadult;
        this.nbadult = nbadult;
        propertyChangeSupport.firePropertyChange(PROP_NBADULT, oldNbadult, nbadult);
    }
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public static final String PROP_NBETUD = "nbetud";

    public int getNbetud() {
        return nbetud;
    }

    public void setNbetud(int nbetud) {
        int oldNbetud = this.nbetud;
        this.nbetud = nbetud;
        propertyChangeSupport.firePropertyChange(PROP_NBETUD, oldNbetud, nbetud);
    }
        
     public static final String PROP_NBENF = "nbenf";

    public int getNbenf() {
        return nbenf;
    }

    public void setNbenf(int nbenf) {
        int oldNbenf = this.nbenf;
        this.nbenf = nbenf;
        propertyChangeSupport.firePropertyChange(PROP_NBENF, oldNbenf, nbenf);
    }
   
     public static final String PROP_PROMO = "promo";

    public boolean isPromo() {
        return promo;
    }

    public void setPromo(boolean promo) {
        boolean oldPromo = this.promo;
        this.promo = promo;
        propertyChangeSupport.firePropertyChange(PROP_PROMO, oldPromo, promo);
    }

    public static final String PROP_MONTANT = "montant";

    public float getMontant() {
        return montant;
    }

    public void setMontant(float montant) {
        float oldMontant = this.montant;
        this.montant = montant;
        propertyChangeSupport.firePropertyChange(PROP_MONTANT, oldMontant, montant);
    }

    public static final String PROP_PRIXMOY = "prixmoy";

    public float getPrixmoy() {
        return prixmoy;
    }

    public void setPrixmoy(float prixmoy) {
        float oldPrixmoy = this.prixmoy;
        this.prixmoy = prixmoy;
        propertyChangeSupport.firePropertyChange(PROP_PRIXMOY, oldPrixmoy, prixmoy);
    }
        
        
        
}
